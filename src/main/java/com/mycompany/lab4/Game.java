/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author Diarydear
 */
import java.util.Scanner;
public class Game {
private Table table;
    private Player player1, player2;
    private char cont = 'y';

    public Game() {
        player1 = new Player('O');
        player2 = new Player('X');
    }
    
    private void newGame() {
        if (cont == 'y') {
            this.table = new Table(player1, player2);
        }
    }
    
    public void play() {
        showWelcome();
        newGame();
        while (checkContinue()) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                showWin();
                inputContinue();
                checkContinue();
                newGame();
            }
            else if (table.checkDraw()) {
                showTable();
                showDraw();
                inputContinue();
                checkContinue();
                newGame();
            }
            table.switchPlayer();
        }
        printEnd();
    }
    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col : ");
            int row = sc.nextInt();
            int col = sc.nextInt();
            table.setRowCol(row, col);
            char[][] t = table.getTable();
            if (t[row - 1][col - 1] == table.getCurrentPlayer().getSymbol()) {
                table.setRowCol(row, col);
                break;
            }
        }
    }
    
    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }
    private void showWin() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!");
    }
    private void showDraw() {
        System.out.println("Draw!!");
    }
    private boolean checkContinue() {
        if (cont == 'y') {
            return true;
        }
        return false;
    }
    private void inputContinue() {
        System.out.print("Continue (y/n): ");
        Scanner sc = new Scanner(System.in);
        char c = sc.next().charAt(0);
        cont = c;
    }
    
    private void printEnd() {
        System.out.print("End Game");
    }

}
